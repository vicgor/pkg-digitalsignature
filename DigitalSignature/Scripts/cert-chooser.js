﻿//**Certificate chooser state**
App.Models.CertChooserState = Backbone.Model.extend({
    //Default values.
    defaults: function () {
        return {
            certList: [],
            currentCertID: 0
        };
    }
});

App.Models.Instances.certChooserState = new App.Models.CertChooserState();

//Subscribe this model on `updateCertChooserState` event.
App.Models.Instances.certChooserState.on("updateCertChooserState", function () {
    var self = this;
    //set default values
    self.set("certList", self.defaults().certList);
    self.set("currentCertID", self.defaults().currentCertID);

    self.trigger("request");

    App.Views.Instances.certChooserView.cryptoAPI.getCerts(
	    function (response) {
	        self.set("certList", $.parseJSON(response).CertificateList);
	        self.set("currentCertID", self.get("certList")[self.get("currentCertID")].CertificateID);
	        self.trigger("sync", response);
	    },
        function (response) {
           self.trigger("error", response);
        }
    );
});

//**CertChooserView**
App.Views.CertChooserView = Backbone.View.extend({
    //DOM element.
    el: $("#certChooser"),

    cryptoAPI: "",

    events: {
        "click .no-data-refresh": "refresh",
        "click .ok-select-cert": "onAccept",
        "click .close-select-cert": "onClose",
        "keypress #pwd": "enterPressed"
    },

    //Initialize model.
    initialize: function () {
        this.cryptoAPI = new CryptoAPI();
        // Subscribe on `sync` event.
        this.model.on('sync', this.sync, this);
        // Subscribe on `request` event.
        this.model.on('request', this.request, this);
        // Subscribe on `error` event.
        this.model.on('error', this.error, this);
    },

    refresh: function (event) {
        this.model.trigger("updateCertChooserState");
    },

    //View templates.
    templates: function (name) {
        var _templates = {
            "loading": "Scripts/templates/loading.html",
            "cert-chooser": "Scripts/templates/certificate-list.html"
        };
        return App.path + _templates[name];
    },

    show:function(){
        this.renderCertChooser();
    },

    renderCertChooser: function () {
        Logger.log("Render CertChooser");

        var kendoWindow = $(this.el).kendoWindow({
            width: "500px",
            resizable: false,
            title: "Выберите сертификат",
            modal: true
        }).data("kendoWindow");

        kendoWindow.center();
        kendoWindow.open();
        $(this.el).show();

        this.model.trigger("updateCertChooserState");

        return this;
    },

    onClose: function(){
        var kendoWindow = $(this.el).data("kendoWindow");
        kendoWindow.close();
    },

    onAccept: function(){
        var pwd = $("#pwd").val();
        var kendoWindow = $(this.el).data("kendoWindow");
        
        var certNum = $("#certList option:selected", $(this.el)).val();
        var certList = this.model.get("certList");
        this.model.set("currentCertID", certList[certNum].CertificateID);

        $(this.el).trigger("accepted", { "certID": this.model.get("currentCertID"), "password": pwd});

        kendoWindow.close();
    },

    enterPressed: function (event) {
        if (event.which == 13 || event.keyCode == 13) {
            this.onAccept();
            return false;
        }
    },

    request: function () {
        Logger.log("Request certChooserView");

        // create DOM element for loader
        var kendoWindow = $(this.el).data("kendoWindow");
        kendoWindow.content("<div class='loading-wrapper'></div>");
        $(".loading-wrapper").after("<div class='error-message-block'></div>");
        //Add ajax loader.
        $(".loading-wrapper", this.el).html(TemplateManager.get(this.templates("loading"))());
        //Empty `error-message-block` block
        $(".error-message-block").empty();
    },

    sync: function () {
        Logger.log("Sync certChooserView");
        //Remove ajax loader.
        $(".loading-wrapper").remove();
        //Empty `error-message-block` block
        $(".error-message-block", $(this.el)).empty();

        var kendoWindow = $(this.el).data("kendoWindow");
        kendoWindow.content(TemplateManager.get(this.templates("cert-chooser"))());

        var certList = this.model.get("certList");

        var select = $('#certList');
        var options = select.prop('options');
        $.each(certList, function (val, text) {
            options[options.length] = new Option(text.CertificateName, val);
        });

        var self = this;

        $("#certList", $(this.el)).kendoComboBox();
        $("input", $("#certList").parent()).attr("readonly", "readonly");
        $("#pwd").focus();
    },

    error: function (xhr) {
        Logger.log("Error certChooserView");

        $(".loading").hide();
        ErrorProcessor.showErrorOnPage(xhr, $(".error-message-block", this.el));
    }
});
App.Views.Instances.certChooserView = new App.Views.CertChooserView({ model: App.Models.Instances.certChooserState });
